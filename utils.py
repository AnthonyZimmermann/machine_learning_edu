import numpy as np
import math
import skimage.io as skiio
import skimage.transform as skit
import skimage.color as skic
import os

class ImageIO(object):

    @staticmethod
    def read(filename, flatten=False, auto_downscale=True, normalization=True, remove_alpha=True):
        if not os.path.isfile(filename):
            print("ERROR: could not open file {}".format(filename))
            return None
        img = np.array(skiio.imread(filename))
        if normalization:
            img = img-img.min()
            img = img/img.max()
        else:
            maxval = (2**math.ceil(math.log(img.max())/math.log(2)))-1 # e.g. 0 to 255 instead of 0 to 256
            img = img/maxval

        if remove_alpha:
            if len(img.shape) == 3 and img.shape[-1] == 4:
                img = skic.rgba2rgb(img)

        if auto_downscale:
            factor = 1
            maxSize = 2000
            if img.shape[0] > maxSize:
                factor = min(factor, maxSize/img.shape[0])
            if img.shape[1] > maxSize:
                factor = min(factor, maxSize/img.shape[1])
            if factor < 1:
                img = skit.rescale(img, factor, mode="constant", anti_aliasing=True, multichannel=True)

        if flatten:
            colorChannels = img.shape[-1]
            img = img.reshape(-1, colorChannels)


        return img

    @staticmethod
    def save(filename, imagedata):
        skiio.imsave(filename, imagedata)
