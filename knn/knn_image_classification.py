import sys
sys.path.insert(0,"..")

from utils import ImageIO
import argparse
import numpy as np
import math
import pickle
import tqdm
import skimage.transform as skit

import matplotlib.pyplot as plt

parser = argparse.ArgumentParser(description="k-nn density estimation for image data")

parser.add_argument('file', metavar="IMAGE", type=str, help="filename of the image")
parser.add_argument('-k', metavar="K", type=int, default=3, help="hyper parameter k (default=3)")
parser.add_argument('-d', '--datasets', metavar="DATASETS", type=str, nargs='+', help="datasets for knn comparison")

def getKNearestNeighbors(data, point, k):
    data = data.reshape(-1,data.shape[-1])

    dists_squared = np.sum(np.power(data-point,2), axis=1)
    #dists = np.sqrt(dists_squared)
    indices = np.argsort(dists_squared)[:k]
    return data[indices]

def loadDatasets(datasets):
    ret = []
    for d in datasets:
        with open(d, "rb") as f:
            ret.append(pickle.load(f))
    return ret

def classify(img, datasets, k):
    img_flat = img.reshape(-1,img.shape[-1])

    datasets_data = loadDatasets(datasets)

    img_flat_classified = np.zeros(img_flat.shape[0], dtype=np.uint8)
    for p_idx,p in enumerate(tqdm.tqdm(img_flat)):
        nearest = None
        for d in datasets_data:
            if nearest is None:
                nearest = getKNearestNeighbors(d,p,k)
            else:
                nearest = np.concatenate((nearest,getKNearestNeighbors(d,p,k)), axis=0)
        n_idx = np.argsort(np.sum(np.power(nearest-p,2), axis=1))[:k]
        n_idx = np.array(n_idx/k, dtype=int)
        occurences = np.zeros(len(datasets_data))
        for i in n_idx:
            occurences[i] = occurences[i] + 1
        img_flat_classified[p_idx] = np.argmax(occurences)

    h,w,*_ = img.shape
    return (img_flat_classified.reshape(h,w)/img_flat_classified.max()).astype(np.uint8)

if __name__ == "__main__":
    args = parser.parse_args()

    inputFilename = args.file
    k = args.k
    datasets = args.datasets

    img = ImageIO.read(inputFilename, normalization=False)
    img = skit.rescale(img, 0.1, mode="constant", anti_aliasing=True, channel_axis=-1)

    out = classify(img, datasets, k)

    ImageIO.save("{}_classified_{}.jpg".format(inputFilename,"_".join(datasets).replace(".data","").replace(".jpg","").replace(".png","")), out)

    fig = plt.figure()
    plt.subplot(121)
    plt.imshow(img)
    plt.subplot(122)
    plt.imshow(out)
    plt.show()
