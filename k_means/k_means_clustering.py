import numpy as np
import matplotlib.pyplot as plt

points1 = np.random.random((20,2))
points1[:,0]  += 0.9
points1[:,1] += -0.2

points2 = np.random.random((20,2))
points2[:,0] += -0.1
points2[:,1] += 0.6

points = np.concatenate((points1,points2))


dimensions = points.shape[1]
k = 2

means = np.random.random((k,dimensions))

def k_means_iteration(points,means):
    dists = np.zeros((points.shape[0],means.shape[0]))
    for idx,mean in enumerate(means):
        diff = points-mean
        dists[:,idx] = np.hypot(diff[:,0],diff[:,1])

    classes = np.argmin(dists,axis=1)

    new_means = np.copy(means)
    for idx in range(means.shape[0]):
        points_class = np.take(points,np.where(classes==idx)[0],axis=0)
        points_class_n = points_class.shape[0]
        if points_class_n == 0:
            continue
        new_means[idx,0] = np.sum(points_class[:,0])/points_class_n
        new_means[idx,1] = np.sum(points_class[:,1])/points_class_n

    return new_means,classes

for i in range(20):
    means,classes = k_means_iteration(points,means)

plt.plot(points[:,0],points[:,1], 'o', means[:,0],means[:,1], 'rx')
plt.show()
