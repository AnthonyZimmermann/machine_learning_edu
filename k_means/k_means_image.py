#!/usr/bin/env python3

import argparse
import skimage.io as skiio
import skimage.color as skic
import skimage.transform as skit
import numpy as np
import sys
import tqdm

parser = argparse.ArgumentParser(description="k-means clustering to cartoonize image data")

parser.add_argument('file', metavar="PNG_IMAGE", type=str, help="png image file")
parser.add_argument('-k', metavar="NUMBER_CLUSTERS", type=int , help="number of color clusters")
parser.add_argument('-i', metavar="MAX_ITERATIONS", type=int , help="max number of iterations")
parser.add_argument('-e', metavar="EPSILON", type=float, help="threshold to stop search")
parser.add_argument('-m', '--mode', metavar="MODE", type=str, help="either normal(default e.g. rgb if images was rgb encoded), 'lab', 'xyz', 'hsv' or 'all' for a combination of all possible")
parser.add_argument('-s', '--samples', metavar="NUMBER_SAMPLES", type=int, help="number of random samples for training (default 0: take all samples into account)")

def iteration(data, means):
    dim = data.shape[-1]
    data = data.reshape(-1,dim)
    num_points = data.shape[0]
    k = means.shape[0]

    # calc distances
    dists = np.zeros(num_points*k).reshape(-1,k)
    for idx,m in enumerate(means):
        dists[:,idx] = np.sqrt(np.sum(np.power(data-m,2), axis=1))

    # assign classes
    classes_idx = np.argmin(dists, axis=1)

    # calc new means
    newMeans = np.zeros_like(means, dtype=float)
    for idx in range(k):
        kPoints = np.take(data, np.where(classes_idx==idx), axis=0).reshape(-1,dim)
        num_kPoints = kPoints.shape[0]
        if num_kPoints > 0:
            newMeans[idx,:] = np.sum(kPoints, axis=0)/num_kPoints

    return newMeans

def classify(data, means):
    dim = data.shape[-1]
    shape_data = data.shape
    data = data.reshape(-1,dim)
    num_points = data.shape[0]
    k = means.shape[0]

    # calc distances
    dists = np.zeros(num_points*k).reshape(-1,k)
    for idx,m in enumerate(means):
        dists[:,idx] = np.sum(np.power(data-m,2), axis=1)

    # assign classes
    classes_idx = np.argmin(dists, axis=1)

    classifiedData = np.array(means[classes_idx], dtype=data.dtype)
    classifiedData = classifiedData.reshape(*shape_data)
    return classifiedData

if __name__ == "__main__":
    args = parser.parse_args()

    inputFilename = args.file
    k = args.k
    max_iterations = args.i
    epsilon = args.e
    mode = args.mode
    samples = args.samples

    if k is None:
        k = 2
    if max_iterations is None:
        max_iterations = 500
    if epsilon is None:
        epsilon = 10**-5
    if mode is None:
        mode = "normal"
    if samples is None:
        samples = 0

    # img.shape = (rows,cols,color_dims)
    img = np.array(skiio.imread(inputFilename))
    img_dtype = img.dtype


    normalization = 0
    # normalization to 0...1
    maxval = np.max(img)
    while maxval> 2**normalization:
        normalization = normalization+1
    img = img/(2**normalization)
    epsilon = epsilon/(2**normalization)

    scale = 1
    if img.shape[0] > 2000:
        scale = min(scale, 2000/img.shape[0])
    if img.shape[1] > 2000:
        scale = min(scale, 2000/img.shape[1])
    if scale < 1:
        img = skit.rescale(img, scale, mode="constant", anti_aliasing=True, channel_axis=-1)

    # copy image for classification if necessary
    img_classification = img

    # copy img and collapse image into 1 dimension with color vectors
    img_training = np.array(img)
    colorChannels = img_training.shape[-1]
    img_training = img_training.reshape(-1,colorChannels)

    if samples > 0:
        minRealisticSamples = k*10**3
        samples = max(minRealisticSamples, samples)
        if samples < img_training.shape[0]:
            print("randomly sample {} samples".format(samples), end=" ", flush=True)
            print("randomize...", end=" ", flush=True)
            np.random.shuffle(img_training)
            print("sample...")
            img_training = img_training[:samples]

    # reshape image so that skimage can transform the "2D image"
    img_training = img_training.reshape(-1,1,colorChannels)

    possibleModes = ["lab","hsv","xyz","all"]
    if mode != "normal" and mode in possibleModes:

        if colorChannels == 4:
            img_training = skic.rgba2rgb(img_training)
            img_classification = skic.rgba2rgb(img_classification)
        elif colorChannels > 4:
            print("{} encoded with too many color channels, only normal mode(rgb) is possible".format(inputFilename))
            sys.exit(1)

        if mode == "lab":
            print("transformation into lab")
            img_training = skic.rgb2lab(img_training)
            img_classification = skic.rgb2lab(img_classification)
            colorChannels = 3

        if mode == "hsv":
            print("transformation into hsv")
            img_training = skic.rgb2hsv(img_training)
            img_classification = skic.rgb2hsv(img_classification)
            colorChannels = 3

        if mode == "xyz":
            print("transformation into xyz")
            img_training = skic.rgb2xyz(img_training)
            img_classification = skic.rgb2xyz(img_classification)
            colorChannels = 3


        if mode == "all":
            print("transformation into lab")
            print("transformation into hsv")
            print("transformation into xyz")
            colorChannels = 12
            redundantImg_training = np.zeros([img_training.shape[0],1,colorChannels], dtype=float)
            redundantImg_training[:,:,0:3] = img_training
            redundantImg_training[:,:,3:6] = skic.rgb2lab(img_training)
            redundantImg_training[:,:,6:9] = skic.rgb2hsv(img_training)
            redundantImg_training[:,:,9:12] = skic.rgb2xyz(img_training)

            img_classification_shape = list(img_classification.shape[:2]) + [colorChannels]
            redundantImg_classification = np.zeros(img_classification_shape, dtype=float)
            redundantImg_classification[:,:,0:3] = img_classification
            redundantImg_classification[:,:,3:6] = skic.rgb2lab(img_classification)
            redundantImg_classification[:,:,6:9] = skic.rgb2hsv(img_classification)
            redundantImg_classification[:,:,9:12] = skic.rgb2xyz(img_classification)

            img_training = redundantImg_training
            img_classification = redundantImg_classification

    means = np.random.random([k,colorChannels])*np.max(img_training)

    print("start iterations")
    for i in tqdm.tqdm(range(max_iterations)):
        newMeans = iteration(img_training, means)
        max_delta = np.max(np.sqrt(np.sum(np.power(newMeans-means,2), axis=1)))
        means = newMeans
        if max_delta < epsilon:
            print("mean correction < {}".format(epsilon*(2**normalization)))
            break
        if i == max_iterations-1:
            print("max number of iterations reached, last mean correction: deltaMean={}".format(max_delta))

    newImg = classify(img_classification, means)

    if mode == "lab":
        print("backtranfsormation from lab")
        newImg = skic.lab2rgb(newImg)
    if mode == "hsv":
        print("backtranfsormation from hsv")
        newImg = skic.hsv2rgb(newImg)
    if mode == "xyz":
        print("backtranfsormation from xyz")
        newImg = skic.xyz2rgb(newImg)

    if mode == "all":
        newImg = newImg[:,:,:3]

    if normalization > 0:
        img = np.array(img*(2**normalization), dtype=img_dtype)
        newImg = np.array(newImg*(2**normalization), dtype=img_dtype)

    outFilename = "{}_{}_k{}.png".format(inputFilename.replace(".png",""), mode, k)
    skiio.imsave(outFilename, newImg)

    import matplotlib.pyplot as plt
    plt.imshow(img)
    plt.show()

    plt.imshow(newImg)
    plt.show()

