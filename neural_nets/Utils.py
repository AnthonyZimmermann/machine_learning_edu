from collections import OrderedDict as odict

class AverageTracker(object):
    def __init__(self):
        self.current_value = dict()
        self.counter = dict()
        self.sum = dict()
        self.sum_abs = dict()

    def reset(self, key="default"):
        self.current_value[key] = 0
        self.counter[key] = 0
        self.sum[key] = 0
        self.sum_abs[key] = 0

    def update(self, key="default", value=None):
        if value is None:
            return
        self.current_value[key] = value
        self.counter[key] += 1
        self.sum[key] += self.current_value[key]
        self.sum_abs[key] += abs(self.current_value[key])

    def getAverage(self, key):
        return self.sum[key] / self.counter[key]

    def getAbsAverage(self, key):
        return self.sum_abs[key] / self.counter[key]

class TablePrinter(object):
    def __init__(self):
        self.defaultValue = ""
        self.fields = odict()

    def get_placeholder(self, length, orientation="left"):
        pstring = ""
        if orientation == "right":
            pstring = ":>{}".format(length)
        elif orientation == "middle":
            pstring = ":^{}".format(length)
        else: # orientation == "left":
            pstring = ":<{}".format(length)
        return " {"+pstring+"} "

    def append_field(self, key, length=10, orientation="left"):
        if key in self.fields.keys():
            print("could not append field because key is already in table")
            return
        self.fields[key] = {"length": length, "orientation": orientation, "value": self.defaultValue, "placeholder": self.get_placeholder(length, orientation)}

    def set_field(self, key, value):
        self.fields[key]["value"] = value

    def clear_field(self, key):
        self.fields[key]["value"] = self.defaultValue

    def print_h_seperation(self):
        pstring = "+"
        for key,f in self.fields.items():
            pstring += "-"*(f["length"]+2) + "+" # +2 is for leading and trailing white space
        print(pstring)

    def flush_headline(self):
        self.print_h_seperation()
        pstring = "|"
        for key,f in self.fields.items():
            pstring += f["placeholder"].format(key) + "|"
        print(pstring)
        self.print_h_seperation()

    def flush_line(self, seperator=True):
        pstring = "|"
        for key,f in self.fields.items():
            pstring += f["placeholder"].format(f["value"]) + "|"
        print(pstring)

        if seperator:
            self.print_h_seperation()
