import TestPlot
import Dataset2d
import DataGenerator2d
import LossFunctions
import Net_2i_3o_1

import Utils

import numpy as np

if __name__ == "__main__":
    dataset = Dataset2d.Dataset2d()
    dataset.appendData("red", DataGenerator2d.Gauss.generatePoints(100, [5,3], np.eye(2)*0.7))
    dataset.appendData("green", DataGenerator2d.Gauss.generatePoints(100, [2,1], np.eye(2)*0.6))
    blue1 = DataGenerator2d.Gauss.generatePoints(50, [2,4], np.eye(2)*0.3)
    blue2 = DataGenerator2d.Gauss.generatePoints(25, [5,0], np.eye(2)*0.1)
    blue3 = DataGenerator2d.Gauss.generatePoints(20, [-2,-1], np.eye(2)*0.8)
    blue4 = DataGenerator2d.Gauss.generatePoints(5, [-1,2], np.eye(2)*0.01)
    blue = np.concatenate((blue1,blue2,blue3,blue4), axis=0)
    dataset.appendData("blue", blue)

    iPlot = TestPlot.InteractivePlot()
    iPlot.plot(dataset.getData("red",0), dataset.getData("red",1), "ro")
    iPlot.plot(dataset.getData("green",0), dataset.getData("green",1), "go")
    iPlot.plot(dataset.getData("blue",0), dataset.getData("blue",1), "bo")
    iPlot.show()

    net = Net_2i_3o_1.Net_2i_3o_1(numHiddenNodes=10)

    loss_f = LossFunctions.L2()

    avg_tracker = Utils.AverageTracker()
    table_printer = Utils.TablePrinter()
    table_printer.append_field("#It", 5, "middle")
    table_printer.append_field("abs epoch loss", 15, "right")
    table_printer.append_field("momentum", 15, "middle")
    table_printer.flush_headline()

    adaption_threshold_momentum = np.inf

    for it in range(1000):
        table_printer.set_field("#It", it)
        avg_tracker.reset("loss")
        eps_loss = 0.0001

        train_data, train_targets = dataset.getAllData()
        #print(train_data, train_targets)

        mini_batch_size = 3
        mini_batch_size = min(mini_batch_size, train_data.shape[0])

        for batch_idx in range(int(train_data.shape[0]/mini_batch_size)):
            avg_tracker.reset("loss_gradient")

            idx0 = batch_idx*mini_batch_size
            idx1 = (batch_idx+1)*mini_batch_size
            batch_data = train_data[idx0:idx1]
            batch_targets = train_targets[idx0:idx1]

            for d,t in zip(batch_data,batch_targets):
                out = net.f_prop(d)

                avg_tracker.update("loss", loss_f.f_prop(out, t))
                avg_tracker.update("loss_gradient", loss_f.b_prop(out, t))
                #print("loss", avg_tracker.current_value["loss"])
                #print("loss_gradient", avg_tracker.current_value["loss_gradient"])

            net.b_prop(avg_tracker.getAverage("loss_gradient"))

        avg_loss = np.sum(avg_tracker.getAverage("loss"))

        if avg_loss < eps_loss:
            break

        if avg_loss < adaption_threshold_momentum:
            old_threshold = adaption_threshold_momentum
            adaption_threshold_momentum = 0.9 * avg_loss

            if old_threshold != np.inf:
                current_momentum = net.optimizer.momentum
                net.optimizer.momentum = 1 - (0.9 * (1-current_momentum))

            table_printer.set_field("momentum", "{:.6f}".format(net.optimizer.momentum))


        table_printer.set_field("abs epoch loss", "{:.6f}".format(avg_loss))
        table_printer.flush_line()


    def getClassColor(point):
        out = net.f_prop(point)
        color_codes = ["r","g","b"]
        idx = np.argmax(out)
        return color_codes[idx]

    testPoints = dict()
    gridX = np.linspace(train_data[:,0].min(),train_data[:,0].max(), 40)
    gridY = np.linspace(train_data[:,1].min(),train_data[:,1].max(), 40)
    idxs,idys = np.meshgrid(gridX,gridY)
    for ix,iy in zip(idxs.reshape(-1), idys.reshape(-1)):
        point = [ix,iy]
        color_code = getClassColor(point)
        if color_code not in testPoints.keys():
            testPoints[color_code] = []
        testPoints[color_code].append(point)

    for k,points in testPoints.items():
        points = np.array(points)
        iPlot.plot(points[:,0],points[:,1], "{}x".format(k))

    iPlot.colorFunction = getClassColor
    iPlot.show()
