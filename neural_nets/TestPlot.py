import matplotlib.pyplot as plt
import numpy as np

class TestPoint(object):
    def __init__(self, p, colorFunction=None):
        self.point = p
        self.xs = [self.point.get_xdata()]
        self.ys = [self.point.get_ydata()]
        self.cid = self.point.figure.canvas.mpl_connect('button_press_event', self)
        if colorFunction is not None:
            self.colorFunction = colorFunction
        else:
            self.colorFunction = lambda point : "black"

    def __call__(self, event):
        if event.inaxes != self.point.axes:
            return
        self.xs = [event.xdata]
        self.ys = [event.ydata]
        self.point.set_xdata(self.xs)
        self.point.set_ydata(self.ys)
        p = np.array([self.xs[0], self.ys[0]])
        self.point.set_color(self.colorFunction([self.xs[0], self.ys[0]]))
        self.point.figure.canvas.draw()

class InteractivePlot(object):
    def __init__(self, colorFunction=None):
        self.colorFunction = colorFunction

        self.plots = []

    def show(self):
        self.fig, self.ax = plt.subplots()

        point, = self.ax.plot([0],[0], "o")
        testPoint = TestPoint(point, self.colorFunction)

        for args,kwargs in self.plots:
            self.ax.plot(*args, **kwargs)

        plt.show()

    def clear(self):
        self.plots = []

    def plot(self, *args, **kwargs):
        self.plots.append((args, kwargs))
