import numpy as np

class Gauss(object):

    @staticmethod
    def generatePoints(num, mean, covariance):
        return np.random.multivariate_normal(mean, covariance, num)
