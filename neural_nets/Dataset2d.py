from collections import OrderedDict as odict
import numpy as np

class Dataset2d(object):
    def __init__(self):
        self.data = odict()

    def appendData(self, label, vectors):
        self.data[label] = np.array(vectors)

    def getData(self, label, dimension=None):
        if dimension is not None and type(dimension) is int:
            return self.data[label][:,dimension]
        return self.data[label]

    def getAllData(self, shuffle=True):
        all_data = None
        class_ranges = []
        index = 0
        for label,vectors in self.data.items():
            vectors = np.array(vectors)
            if all_data is None:
                all_data = vectors
            else:
                all_data = np.concatenate((all_data, vectors), axis=0)
            index_end = index+vectors.shape[0]
            class_ranges.append([index, index_end])
            index = index_end
        targets = np.zeros([all_data.shape[0], len(class_ranges)])
        for idx,r in enumerate(class_ranges):
            r0,r1 = r
            targets[r0:r1,idx] = 1

        if shuffle:
            comb = np.concatenate((all_data,targets), axis=1)
            np.random.shuffle(comb)
            all_data = comb[:,:all_data.shape[1]]
            targets = comb[:,all_data.shape[1]:]

        return all_data, targets

