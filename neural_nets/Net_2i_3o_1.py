import Layers
import Activations
import Optimizer
import Regularizer

class Net_2i_3o_1(object):
    def __init__(self, **kwargs):
        defaultNumHiddenNodes = 20

        self.optimizer = Optimizer.SGD(lr=0.05, momentum=0.01)
        self.regularizer = Regularizer.L2(coefficient=0.001)

        self.fc1 = Layers.FC(2,kwargs.get("numHiddenNodes", defaultNumHiddenNodes))
        self.fc1.initDetails["w_mode"] = "normal"
        self.fc1.initDetails["w_mean"] = 0
        self.fc1.initDetails["w_deviation"] = 1
        self.fc1.optimizer = self.optimizer
        self.fc1.regularizer = self.regularizer
        self.fc1.init()

        self.a1 = Activations.Sigmoid()

        self.fc2 = Layers.FC(kwargs.get("numHiddenNodes", defaultNumHiddenNodes), 3)
        self.fc2.initDetails["w_mode"] = "normal"
        self.fc2.initDetails["w_mean"] = 0
        self.fc2.initDetails["w_deviation"] = 1
        self.fc2.optimizer = self.optimizer
        self.fc2.regularizer = self.regularizer
        self.fc2.init()

        self.a2 = Activations.Sigmoid()

    def f_prop(self, vector):
        y1 = self.fc1.f_prop(vector)
        z1 = self.a1.f_prop(y1)

        y2 = self.fc2.f_prop(z1)
        z2 = self.a2.f_prop(y2)

        return z2

    def b_prop(self, gradient):
        gz2 = self.a2.b_prop(gradient)
        gy2 = self.fc2.b_prop(gz2)

        gz1 = self.a1.b_prop(gy2)
        gy1 = self.fc1.b_prop(gz1)
        return gy1
