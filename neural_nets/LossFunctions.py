import numpy as np

# 1/2 * sum((out-target)**2)
class L2(object):
    def __init__(self):
        pass

    def f_prop(self, output, target):
        diff = output.reshape(-1,1) - target.reshape(-1,1)
        return 0.5 * diff * diff

    def b_prop(self, output, target):
        diff = output.reshape(-1,1) - target.reshape(-1,1)
        return diff

# ??? class L1(object):
# ???     def __init__(self):
# ???         pass
# ???
# ???     def f_prop(self, output, target):
# ???         return abs(output - target)
# ???
# ???     def b_prop(self, output, target):
# ???         return np.sign(output)

# ??? class Softmax(object):
# ???     def __init__(self):
# ???         pass
# ???
# ???     def f_prop(self, output, target):
# ???         pass
# ???
# ???     def b_prop(self, output, target):
# ???         pass
