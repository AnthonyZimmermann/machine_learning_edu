import numpy as np

class SGD(object):
    def __init__(self, lr, momentum=None):
        self.lr = lr
        self.momentum = momentum
        self.velocity = dict()
        self.variables = dict()

    def addVariables(self, obj, variables):
        self.variables[obj] = variables
        if self.momentum is not None:
            self.velocity[obj] = np.zeros_like(variables)

    def update(self, obj, gradients):
        if self.momentum is not None:
            self.velocity[obj] = (self.momentum * self.velocity[obj]) - (self.lr * gradients)
            delta = self.velocity[obj]
        else:
            delta = -self.lr * gradients

        self.variables[obj] += delta
