import numpy as np

class L2(object):
    def __init__(self, coefficient):
        self.coefficient = coefficient
        self.variables = dict()

    def addVariables(self, obj, variables):
        self.variables[obj] = variables

    def getRegularization(self, obj):
        return self.coefficient*np.copy(self.variables[obj])
