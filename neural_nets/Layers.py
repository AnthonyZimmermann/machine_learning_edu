import Optimizer
import numpy as np

class FC(object):
    def __init__(self, numInputs, numOutputs):
        self.numIn = numInputs
        self.numOut = numOutputs

        self.initDetails = dict()
        self.initDetails["w_mode"] = "normal"
        self.initDetails["w_deviation"] = 0.1
        self.initDetails["w_mean"] = 0

        self.initDetails["b_mode"] = "normal"
        self.initDetails["b_deviation"] = 0.1
        self.initDetails["b_mean"] = 0

        self.optimizer = None
        self.regularizer = None

    def init(self):
        self.init_weights()

        self.init_optimizer()
        self.init_regularizer()

    def init_weights(self):
        # init weights
        if self.initDetails["w_mode"] == "uniform":
            deviation = self.initDetails["w_deviation"]
            mean = self.initDetails["w_mean"]
            # 1 weight more in input dimension to include bias
            self.weights = (np.random.random([self.numOut, self.numIn+1])*2*deviation)-deviation+mean
        else:
            deviation = self.initDetails["w_deviation"]
            mean = self.initDetails["w_mean"]
            self.weights = np.random.normal(mean, deviation, [self.numOut, self.numIn+1])

        # init biases
        if self.initDetails["b_mode"] == "uniform":
            deviation = self.initDetails["b_deviation"]
            mean = self.initDetails["b_mean"]
            self.weights[:,0] = (np.random.random(self.numOut)*2*deviation)-deviation+mean
        else:
            deviation = self.initDetails["b_deviation"]
            mean = self.initDetails["b_mean"]
            self.weights[:,0] = np.random.normal(mean, deviation, self.numOut)

    def init_optimizer(self):
        self.optimizer.addVariables(self, self.weights)

    def init_regularizer(self):
        if self.regularizer is not None:
            self.regularizer.addVariables(self, self.weights[:,1:])

    def f_prop(self, inputs):
        self.inputs = np.array(inputs)
        out = np.copy(self.weights[:,0].reshape(self.numOut,1))
        out += np.dot(self.weights[:,1:], self.inputs.reshape(self.numIn,1))
        return out

    def b_prop(self, gradients):
        w_grad = np.zeros_like(self.weights)
        w_grad[:,1:] = np.copy(gradients.reshape(self.numOut,1))*self.inputs.reshape(1,-1)
        w_grad[:,0] = np.copy(gradients.reshape(self.numOut))

        if self.regularizer is not None:
            w_grad[:,1:] += self.regularizer.getRegularization(self)

        self.update_weights(w_grad)

        inp_grad = np.copy(self.weights[:,1:])*gradients.reshape(self.numOut,1)
        inp_grad = np.sum(inp_grad, axis=0)
        return inp_grad

    def update_weights(self, gradients):
        self.optimizer.update(self, gradients)
