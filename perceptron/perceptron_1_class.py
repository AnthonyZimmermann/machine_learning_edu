import matplotlib.pyplot as plt
import numpy as np

def calcOutput(inputs, weights):
    threshold = 0

    inputsB = np.ones(inputs.shape[0]+1)
    inputsB[1:] = inputs

    if np.sum(inputsB*weights) > threshold:
        return 1
    else:
        return 0

def updateWeights(weights, inputs, factor):

    inputsB = np.ones(inputs.shape[0]+1)
    inputsB[1:] = inputs

    return weights - (factor*inputsB)

if __name__ == "__main__":


    x11 = np.random.normal(3, 1, 100)
    x12 = np.random.normal(2, 0.3, 100)

    x21 = np.random.normal(8, 0.7, 100)
    x22 = np.random.normal(7, 0.3, 100)

    p1 = np.zeros([100,2])
    p1[:,0] = x11
    p1[:,1] = x12

    p2 = np.zeros([100,2])
    p2[:,0] = x21
    p2[:,1] = x22

    p = np.concatenate((p1,p2), axis=0)

    t = np.zeros([200,1])
    t[:100] = 1
    t[100:] = 0


    # 2d input (+bias), 1d output
    weights = np.zeros(3)


    accumulatedDiff = t.shape[0]
    while(accumulatedDiff/t.shape[0] > 10**(-5)):
        accumulatedDiff = 0
        # permuted for loop
        indices = np.arange(p.shape[0])
        np.random.shuffle(indices)
        for idx in indices:
            point = p[idx]
            target = t[idx]

            y = calcOutput(point, weights)

            diff = y - target
            accumulatedDiff += abs(diff)
            weights = updateWeights(weights, point, 0.1*diff)

    fig, ax = plt.subplots()
    ax.plot(p1[:,0], p1[:,1], "ro")
    ax.plot(p2[:,0], p2[:,1], "bo")

    class TestPoint(object):
        def __init__(self, p, weights):
            self.weights = weights
            self.point = p
            self.xs = [self.point.get_xdata()]
            self.ys = [self.point.get_ydata()]
            self.cid = self.point.figure.canvas.mpl_connect('button_press_event', self)

        def __call__(self, event):
            if event.inaxes != self.point.axes:
                return
            self.xs = [event.xdata]
            self.ys = [event.ydata]
            self.point.set_xdata(self.xs)
            self.point.set_ydata(self.ys)
            p = np.array([self.xs[0], self.ys[0]])
            if calcOutput(p, self.weights) > 0:
                self.point.set_color("r")
            else:
                self.point.set_color("b")
            self.point.figure.canvas.draw()

    point, = ax.plot([0],[0], "o")
    testPoint = TestPoint(point, weights)

    plt.show()
