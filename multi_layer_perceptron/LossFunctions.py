import numpy as np

class Rosenblatt(object):
    def __init__(self, threshold=0):
        self.threshold = threshold

    def calc(self, output, target):
        diff = output.reshape(-1,1) - target.reshape(-1,1)

        label = np.where(diff>self.threshold, 1, 0)
        loss = np.where(label==target.reshape(-1,1),0,1)
        loss = loss * np.sign(diff)

        return loss

# 1/2 * sum((out-target)**2)
class L2(object):
    def __init__(self):
        pass

    def calc(self, output, target):
        diff = output.reshape(-1,1) - target.reshape(-1,1)
        return diff
