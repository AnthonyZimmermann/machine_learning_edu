import Layers
import LossFunctions
import TestPlot
import Activations
import numpy as np
import math

if __name__ == "__main__":

    x11 = np.random.normal(3,  1,100)
    x12 = np.random.normal(2.5,0.3,100)
    x21 = np.random.normal(9,0.2,100)
    x22 = np.random.normal(18,0.8,100)
    x31 = np.random.normal(8,0.7,100)
    x32 = np.random.normal(7,0.3,100)

    p1 = np.zeros([100,2])
    p1[:,0] = x11
    p1[:,1] = x12

    p2 = np.zeros([100,2])
    p2[:,0] = x21
    p2[:,1] = x22

    p3 = np.zeros([100,2])
    p3[:,0] = x31
    p3[:,1] = x32

    p = np.concatenate((p1,p2,p3), axis=0)

    t = np.zeros([300,3])
    t[:100,0] = 1
    t[100:200,1] = 1
    t[200:,2] = 1

    fc1 = Layers.FC(2,1000)
    fc1.initDetails["opt_lr"] = 0.2
    fc1.init()
    a1 = Activations.Sigmoid()

    fc2 = Layers.FC(1000,3)
    fc2.initDetails["opt_lr"] = 0.2
    fc2.init()
    a2 = Activations.Sigmoid()

#    f_loss = LossFunctions.Rosenblatt()
    f_loss = LossFunctions.L2()

    accLoss = t.shape[0]
    maxNumIterations = 200
    it = 0
    while accLoss/t.shape[0] > 0.005:
        accLoss = 0
        indices = np.arange(p.shape[0])
        np.random.shuffle(indices)

        for idx in indices:
            point = p[idx]
            target = t[idx]

            y1 = fc1.f_prop(point)
            yy1 = a1.f_prop(y1)
            y2 = fc2.f_prop(yy1)
            yy2 = a2.f_prop(y2)

            loss = f_loss.calc(yy2, target)

            gg2 = a2.b_prop(loss)
            g2 = fc2.b_prop(gg2)
            gg1 = a1.b_prop(g2)
            g1 = fc1.b_prop(gg1)

            accLoss += np.abs(loss)
        accLoss = np.sum(accLoss)
        print("Iteration: {}, Loss: {:.3f}".format(it, accLoss/t.shape[0]), end="\r", flush=True)
        it += 1
        if it > maxNumIterations:
            print("Iteration: {}, Loss: {:.3f}".format(it, accLoss/t.shape[0]))
            print("max num iterations reached")
            break
    print("training done, Iterations: {}, Loss: {}".format(it, accLoss/t.shape[0]))

    def f_prop(point):
        y1 = fc1.f_prop(point)
        yy1 = a1.f_prop(y1)
        y2 = fc2.f_prop(yy1)
        yy2 = a2.f_prop(y2)


        if yy2.shape[0] == 1:
            if yy2 > 0.5:
                return "r"
            else:
                return "g"
        elif yy2.shape[0] == 2:
            if yy2[0] > yy2[1]:
                return "r"
            else:
                return "g"
        elif yy2.shape[0] == 3:
            if yy2[0] > yy2[1] and yy2[0] > yy2[2]:
                return "r"
            elif yy2[1] > yy2[2]:
                return "g"
            else:
                return "b"

    green = []
    red = []
    blue = []
    gridX = np.linspace(p[:,0].min()-10,p[:,0].max()+10, 40)
    gridY = np.linspace(p[:,1].min()-10,p[:,1].max()+10, 40)
    idxs,idys = np.meshgrid(gridX,gridY)
    for ix,iy in zip(idxs.reshape(-1),idys.reshape(-1)):
        point = [ix,iy]
        color = f_prop(point)
        if color == "r":
            red.append(point)
        elif color == "g":
            green.append(point)
        elif color == "b":
            blue.append(point)

    green = np.array(green)
    red = np.array(red)
    blue = np.array(blue)

    import matplotlib.pyplot as plt
    fig = plt.figure()
    if green.shape[0] > 0:
        plt.plot(green[:,0], green[:,1], "gx")
    if red.shape[0] > 0:
        plt.plot(red[:,0], red[:,1], "rx")
    if blue.shape[0] > 0:
        plt.plot(blue[:,0], blue[:,1], "bx")

    testPlot = TestPlot.TestPlot(p1,p2,p3, testFunction=f_prop)
