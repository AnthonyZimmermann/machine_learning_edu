import Optimizer
import numpy as np

class FC(object):
    def __init__(self, numInputs, numOutputs):
        self.numIn = numInputs
        self.numOut = numOutputs

        self.initDetails = dict()
        self.initDetails["w_mode"] = "uniform"
        self.initDetails["w_deviation"] = 0.1
        self.initDetails["w_mean"] = 0

        self.initDetails["b_mode"] = "uniform"
        self.initDetails["b_deviation"] = 0.1
        self.initDetails["b_mean"] = 0

        self.initDetails["optimizer"] = "sgd"
        self.initDetails["opt_lr"] = 0.1

    def init(self):
        self.init_weights()

        self.init_optimizer()

    def init_weights(self):
        # init weights
        if self.initDetails["w_mode"] == "uniform":
            deviation = self.initDetails["w_deviation"]
            mean = self.initDetails["w_mean"]
            # 1 weight more in input dimension to include bias
            self.weights = (np.random.random([self.numOut, self.numIn+1])*2*deviation)-deviation+mean

        # init biases
        if self.initDetails["b_mode"] == "uniform":
            deviation = self.initDetails["b_deviation"]
            mean = self.initDetails["b_mean"]
            self.weights[:,0] = (np.random.random(self.numOut)*2*deviation)-deviation+mean

    def init_optimizer(self):
        if self.initDetails["optimizer"].lower() == "sgd":
            lr = self.initDetails["opt_lr"]
            self.optimizer = Optimizer.SGD(self.weights, lr)

    def f_prop(self, inputs):
        self.inputs = np.array(inputs)
        out = np.copy(self.weights[:,0].reshape(self.numOut,1))
        out += np.dot(self.weights[:,1:], self.inputs.reshape(self.numIn,1))
        return out

    def b_prop(self, gradients):
        w_grad = np.zeros_like(self.weights)
        w_grad[:,1:] = np.copy(gradients.reshape(self.numOut,1))*self.inputs.reshape(1,-1)
        w_grad[:,0] = np.copy(gradients.reshape(self.numOut))

        self.update_weights(w_grad)

        inp_grad = np.copy(self.weights[:,1:])*gradients.reshape(self.numOut,1)
        inp_grad = np.sum(inp_grad, axis=0)
        return inp_grad

    def update_weights(self, gradients):
        self.optimizer.update(gradients)
