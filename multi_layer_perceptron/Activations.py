import numpy as np

class Sigmoid(object):
    def __init__(self):
        self.out = None

    def f_prop(self, inputs):
        out = 1/(1+np.exp(-inputs))
        self.out = out
        return out

    def b_prop(self, gradients):
        inp_grad = (self.out*(1-self.out)).reshape(-1,1) * gradients.reshape(-1,1)
        return inp_grad
