import matplotlib.pyplot as plt
import numpy as np

class TestPoint(object):
    def __init__(self, p, testFunction):
        self.point = p
        self.xs = [self.point.get_xdata()]
        self.ys = [self.point.get_ydata()]
        self.cid = self.point.figure.canvas.mpl_connect('button_press_event', self)
        self.testFunction = testFunction

    def __call__(self, event):
        if event.inaxes != self.point.axes:
            return
        self.xs = [event.xdata]
        self.ys = [event.ydata]
        self.point.set_xdata(self.xs)
        self.point.set_ydata(self.ys)
        p = np.array([self.xs[0], self.ys[0]])
        self.point.set_color(self.testFunction([self.xs[0], self.ys[0]]))
        self.point.figure.canvas.draw()

class TestPlot(object):
    def __init__(self, p1, p2, p3=None, testFunction=None):
        fig, ax = plt.subplots()
        ax.plot(p1[:,0], p1[:,1], "ro")
        ax.plot(p2[:,0], p2[:,1], "go")
        if p3 is not None:
            ax.plot(p3[:,0], p3[:,1], "bo")

        point, = ax.plot([0],[0], "o")
        testPoint = TestPoint(point, testFunction)

        plt.show()
