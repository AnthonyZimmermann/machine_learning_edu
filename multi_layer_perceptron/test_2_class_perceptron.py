import Layers
import LossFunctions
import TestPlot
import numpy as np

if __name__ == "__main__":

    x111 = np.random.normal(3,  1,50)
    x121 = np.random.normal(2.5,0.3,50)
    x112 = np.random.normal(3,0.2,50)
    x122 = np.random.normal(18,0.8,50)
    x11 = np.concatenate((x111,x112), axis=0)
    x12 = np.concatenate((x121,x122), axis=0)

    x21 = np.random.normal(8,0.7,100)
    x22 = np.random.normal(7,0.3,100)

    p1 = np.zeros([100,2])
    p1[:,0] = x11
    p1[:,1] = x12

    p2 = np.zeros([100,2])
    p2[:,0] = x21
    p2[:,1] = x22

    p = np.concatenate((p1,p2), axis=0)

    t = np.zeros([200,2])
    t[:100,0] = 1
    t[100:,1] = 1

    fc = Layers.FC(2,2)
    fc.init()
    f_loss = LossFunctions.Rosenblatt()

    accLoss = t.shape[0]
    while accLoss/t.shape[0] > 10**(-5):
        accLoss = 0
        indices = np.arange(p.shape[0])
        np.random.shuffle(indices)

        for idx in indices:
            point = p[idx]
            target = t[idx]

            out = fc.f_prop(point)
            loss = f_loss.calc(out, target)
            fc.b_prop(loss)

            accLoss += abs(loss)
        accLoss = np.sum(accLoss)
        print(accLoss)

    def f_prop(point):
        out = fc.f_prop(point)
        if out.shape[0] == 1:
            if out > 0:
                return "r"
            else:
                return "b"
        else:
            if out[0] > out[1]:
                return "r"
            else:
                return "b"

    testPlot = TestPlot.TestPlot(p1,p2, testFunction=f_prop)
