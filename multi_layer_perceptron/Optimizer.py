class SGD(object):
    def __init__(self, variables, lr):
        self.variables = variables
        self.lr = lr

    def update(self, gradients):
        self.variables -= self.lr * gradients
