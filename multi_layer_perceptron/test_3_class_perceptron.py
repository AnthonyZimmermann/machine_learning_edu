import Layers
import LossFunctions
import TestPlot
import numpy as np

if __name__ == "__main__":

    x11 = np.random.normal(3,  1,100)
    x12 = np.random.normal(2.5,0.3,100)
    x21 = np.random.normal(9,0.2,100)
    x22 = np.random.normal(18,0.8,100)
    x31 = np.random.normal(8,0.7,100)
    x32 = np.random.normal(7,0.3,100)

    p1 = np.zeros([100,2])
    p1[:,0] = x11
    p1[:,1] = x12

    p2 = np.zeros([100,2])
    p2[:,0] = x21
    p2[:,1] = x22

    p3 = np.zeros([100,2])
    p3[:,0] = x31
    p3[:,1] = x32

    p = np.concatenate((p1,p2,p3), axis=0)

    t = np.zeros([300,3])
    t[:100,0] = 1
    t[100:200,1] = 1
    t[200:,2] = 1

    fc = Layers.FC(2,3)
    fc.initDetails["opt_lr"] = 1
    fc.init()
    f_loss = LossFunctions.Rosenblatt()

    accLoss = t.shape[0]
    maxNumIterations = 10**4
    it = 0
    while accLoss/t.shape[0] > 10**(-5):
        accLoss = 0
        indices = np.arange(p.shape[0])
        np.random.shuffle(indices)

        for idx in indices:
            point = p[idx]
            target = t[idx]

            out = fc.f_prop(point)
            loss = f_loss.calc(out, target)
            fc.b_prop(loss)

            accLoss += np.abs(loss)
        accLoss = np.sum(accLoss)
        print(accLoss)
        it += 1
        if it > maxNumIterations:
            print("max num iterations reached")
            break

    def f_prop(point):
        out = fc.f_prop(point)
        if out.shape[0] == 1:
            if out > 0:
                return "r"
            else:
                return "b"
        elif out.shape[0] == 2:
            if out[0] > out[1]:
                return "r"
            else:
                return "b"
        elif out.shape[0] == 3:
            if out[0] > out[1] and out[0] > out[2]:
                return "r"
            elif out[1] > out[2]:
                return "b"
            else:
                return "g"

    testPlot = TestPlot.TestPlot(p1,p2,p3, testFunction=f_prop)
