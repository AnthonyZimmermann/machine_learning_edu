#!/usr/bin/env python3
import sys
sys.path.insert(0, "..")

import numpy as np
import math
from utils import ImageIO
import tqdm

import matplotlib.pyplot as plt
import argparse

parser = argparse.ArgumentParser(description="expectation maximization using gaussian distributions, to cartoonize an image")

parser.add_argument('file', metavar="IMAGE", type=str, help="image file")
parser.add_argument('-k', metavar="NUMBER_CLUSTERS", type=int, default=3, help="number of distributions to fit")
parser.add_argument('-i', metavar="MAX_ITERATIONS", type=int, default=500, help="max number of iterations")
parser.add_argument('-e', metavar="EPSILON", type=float, default=10**-8, help="threshold for error to stop search")

def random_init(img, k):
    dimensions = img.shape[-1]
    sigma = np.dot(img.T,img)/img.shape[0]
    out = []
    for i in range(k):
        pi = 1/k
        mu = np.random.random(dimensions)
        out.append((pi, mu, sigma))
    return out

def multiVarGauss(points, mu, sigma):
    dimensions = points.shape[-1]
    eps = 10**-9
    sigmaPrime = sigma + np.eye(3)*eps

    # [(2pi)^(D/2) * det(sigma)]^(-1) * exp[ -0.5 * (X-mu)^T * sigma^(-1) * (X-mu) ]
    # |F|                             * exp[ |EXP| ]

    # F:
    f = 1 / ( np.power((2 * math.pi),dimensions/2) * np.power(np.linalg.det(sigmaPrime),0.5) )

    # batches of 10000
    out = np.zeros(points.shape[0])
    batch_size = 10000
    num_batches = int(points.shape[0]/batch_size)
    for i in range(num_batches):

        # EXP:
        xmu = points[i*batch_size:(i+1)*batch_size]-mu
        exp = -0.5 * np.sum((np.dot(xmu, np.linalg.inv(sigmaPrime)) * xmu), axis=1)
        out[i*batch_size:(i+1)*batch_size] = np.exp(exp)

    return f * out

def e_step(img_flat, params):
    softAssignments = np.zeros([len(params), img_flat.shape[0]])
    for idx,p in enumerate(params):
        pi,mu,sigma = p
        softAssignments[idx] = pi * multiVarGauss(img_flat, mu, sigma)

    return softAssignments/np.sum(softAssignments, axis=0)

def m_step(gammas, img_flat):
    params = []
    for gamma_row in gammas:

        softSampleNum = np.sum(gamma_row)
        pi_new = softSampleNum/gamma_row.shape[0]
        mu_new = 1/softSampleNum * np.dot(gamma_row, img_flat)
        mu_new = mu_new.clip(0.0,1.0)

        # batches of 10000
        dimensions = img_flat.shape[-1]
        batch_size = 10000
        num_batches = int(img_flat.shape[0]/batch_size)

        sigma_new = np.zeros(dimensions**2).reshape(dimensions,dimensions)
        xmu = img_flat-mu_new
        for i in range(num_batches):
            gamma_row_batch = gamma_row[i*batch_size:(i+1)*batch_size]
            xmu_batch = xmu[i*batch_size:(i+1)*batch_size]
            gamma_row_xmu_batch = (gamma_row_batch * xmu_batch.T).T
            sigma_new = sigma_new + np.dot(gamma_row_xmu_batch.T, xmu_batch)
        sigma_new = 1/softSampleNum * sigma_new
        params.append((pi_new, mu_new, sigma_new))
    return params

def classify(img, params):
    dimensions = img.shape[-1]
    img_flat = img.reshape(-1, img.shape[-1])

    probs = np.zeros([img_flat.shape[0], len(params)])

    mus = []
    for idx,p in enumerate(params):
        pi,mu,sigma = p
        probs[:,idx] = pi * multiVarGauss(img_flat, mu, sigma)
        mus.append(mu)

    mus = np.array(mus)
    out_flat = mus[np.argmax(probs, axis=1)]
    return out_flat.reshape(*img.shape)

if __name__ == "__main__":
    args = parser.parse_args()

    inFile = args.file
    k = args.k
    max_iterations = args.i
    epsilon = args.e

    img = ImageIO.read(inFile)
    h,w,d = img.shape

    img_flat = np.array(img).reshape(-1,d)
    np.random.shuffle(img_flat)
    img_flat = img_flat[:1000000]

    params = random_init(img_flat, k)

    for i in tqdm.tqdm(range(max_iterations)):
        gammas = e_step(img_flat, params)
        params_new = m_step(gammas, img_flat)
        diff = 0
        for p,pn in zip(params,params_new):
            p_pi,p_mu,p_sigma = p
            pn_pi,pn_mu,pn_sigma = pn

            diff = diff + abs(np.sum(p_pi-pn_pi)) + abs(np.sum(p_mu-pn_mu)) + abs(np.sum(p_sigma-pn_sigma))
        diff = diff/len(params)
        print(diff)
        params = params_new
        if diff < epsilon:
            print("stopped optimization, update difference in sum: {}".format(diff))
            break
        if i == max_iterations-1:
            print("max number of iterations for optimization reached")
            print("last difference of params in sum: {}".format(diff))

    img_classified = classify(img, params)
    img_classified_uint8 = (img_classified*255).astype(np.uint8)

    ImageIO.save("{}_k{}.png".format(inFile.replace(".png","").replace(".jpg",""),k), img_classified_uint8)

    fig = plt.figure()
    plt.subplot(121)
    plt.imshow(img)
    plt.subplot(122)
    plt.imshow(img_classified)
    plt.show()
