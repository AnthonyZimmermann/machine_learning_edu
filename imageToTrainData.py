import skimage.io as skiio
from utils import ImageIO
import numpy as np
import os
import pickle

import argparse

parser = argparse.ArgumentParser(description="convert image data into a dataset, only pixels with alpha>0.95 are considered")
parser.add_argument('files', nargs='+', metavar="IMAGE", type=str, help="image file")
parser.add_argument("-l", "--label", metavar="LABEL", type=str, help="label")
parser.add_argument("-n", "--number", metavar="SAMPLES", type=int, help="number of samples")
parser.add_argument("--normalization", metavar="NORMALIZATION", type=str, help="normalization [on|off] (default=on)")

if __name__ == "__main__":
    args = parser.parse_args()

    inputFilenames = args.files
    label = args.label
    number = args.number
    normalization = args.normalization
    norm = not (normalization == "off")

    data = None
    for inpFile in inputFilenames:
        #rgba
        img = ImageIO.read(inpFile, flatten=True, auto_downscale=False, normalization=norm, remove_alpha=False)

        if img.shape[-1] == 4:
            newData = []
            for p in img:
                if p[-1] > 0.95:
                    newData.append(p)

            newData = np.array(newData)
            newData = newData[:,:3]
        elif img.shape[-1] == 3:
            newData = img

        if data is not None:
            data = np.concatenate((data,newData), axis=0)
        else:
            data = newData

    np.random.shuffle(data)

    if number is not None:
        data = data[:number]

    print(data.shape,data.min(),data.max())
    print("sampled {} datapoints".format(data.shape[0]))
    with open("{}.data".format(label), "wb") as f:
        pickle.dump(data,f)
