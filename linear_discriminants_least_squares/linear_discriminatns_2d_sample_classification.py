import matplotlib.pyplot as plt
import numpy as np
import argparse

parser = argparse.ArgumentParser(description="demo of linear discriminants for a random dummy testset")

parser.add_argument('--outliers', action='store_true')
parser.add_argument('--overlap', action='store_true')

def pseudoInv(xVec):
    return np.dot(np.linalg.inv(np.dot(xVec.T,xVec)), xVec.T)

def decisionBoundary(w,p):
    w10,w11,w12 = w.T[0]
    w20,w21,w22 = w.T[1]

    minx1 = p[:,1].min()
    minx2 = p[:,2].min()
    maxx1 = p[:,1].max()
    maxx2 = p[:,2].max()

    out1 = []
    out1.append([minx1, (0.505-w10-(w11*minx1))/w12])
    out1.append([maxx1, (0.505-w10-(w11*maxx1))/w12])
    out1 = np.array(out1)

    out2 = []
    out2.append([minx1, (0.505-w20-(w21*minx1))/w22])
    out2.append([maxx1, (0.505-w20-(w21*maxx1))/w22])
    out2 = np.array(out2)

    return out1,out2

if __name__ == "__main__":
    args = parser.parse_args()

    outliers = args.outliers
    overlap = args.overlap

    x11 = np.random.normal(3, 1, 100)
    x12 = np.random.normal(2, 0.3, 100)

    x21 = np.random.normal(8, 0.7, 100)
    x22 = np.random.normal(7, 0.3, 100)

    p1 = np.zeros([100,3])
    p1[:,0] = 1
    p1[:,1] = x11
    p1[:,2] = x12

    p2 = np.zeros([100,3])
    p2[:,0] = 1
    p2[:,1] = x21
    p2[:,2] = x22

    if overlap:
        p2[:4,2:] /= 3
        p2[8,1:] /= 8
        p2[9,1:] /= 2

    if outliers:
        if np.random.random() < 0.5:
            p2[20:30,1:] *= 3
        else:
            p1[20:30,1:] *= -0.6

    p = np.concatenate((p1,p2), axis=0)

    t = np.zeros([200,2])
    t[:100,0] = 1
    t[100:,1] = 1


    w = np.dot(pseudoInv(p), t)
    print(w)

    plt.plot(p1[:,1],p1[:,2], "ro")
    plt.plot(p2[:,1],p2[:,2], "bo")
    dB1,dB2 = decisionBoundary(w,p)
    plt.plot(dB1[:,0],dB1[:,1], "r")
    plt.plot(dB2[:,0],dB2[:,1], "b")
    plt.show()
